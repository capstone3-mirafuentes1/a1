// import {useEffect, useState, useContext, Fragment} from 'react'
// import Table from 'react-bootstrap/Table';
// import {useParams, useNavigate} from 'react-router-dom'
// import Swal from 'sweetalert2'
// import ProductCard from '../components/ProductCard.js'


// export default function AddCart(userProp){
// 	const {_id} = userProp
// 	const [name, setName] = useState("")
// 	const [description, setDescription] = useState("")
// 	const [price, setPrice] = useState("")
// 	const navigate = useNavigate()
// 	const [product, setProduct] = useState('')
// 	const {productId} = useParams()


// 	useEffect(() => {
// 		fetch(`${process.env.REACT_APP_API_URL}/order/addCart/${productId}`, {
// 			headers: {
// 				Authorization: `Bearer ${localStorage.getItem("token")}`
// 			}
// 		})
// 		.then(result => result.json())
// 		.then(data => {
// 			console.log(data)
// 			setProduct(data.map(product => {
// 				return(
// 				<ProductCard key = {product._id} productProp = {product}/>
// 				)
// 			}))
// 		})
// 		.catch(error => {
// 			console.log(error)
// 		})
// 	}, [])
		

// 	return(
// 		<Fragment>
// 			<h1 className="text-center mt-3">Cart Content</h1>
// 			<Table striped bordered hover className="text-center">
// 				<thead>
// 					<tr>
// 						<th>Name</th>
// 						<th>Description</th>
// 						<th>Price</th>
// 						<th>Quantity</th>
// 						<th>Subtotal</th>
// 					</tr>
// 				</thead>
				
// 			</Table>				
// 		</Fragment>

// 		)

// }

import { useEffect, useState, useContext, Fragment } from "react";
import Table from "react-bootstrap/Table";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import ProductCard from "../components/ProductCard.js";

export default function AddCart(userProp) {
  const { _id } = userProp;
  const [cartItems, setCartItems] = useState([]); // create state to store cart items
  const { productId } = useParams();

  useEffect(() => {
    // fetch cart items for the user from the server
    fetch(`${process.env.REACT_APP_API_URL}/order/addCart`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((result) => result.json())
      .then((data) => {
        console.log(data);
        setCartItems(data); // update the state with the fetched cart items
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const addToCart = () => {
    // add the selected product to the cart
    fetch(`${process.env.REACT_APP_API_URL}/order/addToCart/${productId}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({ quantity: 1 }), // set the quantity to 1 for now
    })
      .then((result) => result.json())
      .then((data) => {
        console.log(data);
        setCartItems(data); // update the state with the updated cart items
        Swal.fire({
          icon: "success",
          title: "Item added to cart",
          showConfirmButton: false,
          timer: 1500,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <Fragment>
      <h1 className="text-center mt-3">Cart Content</h1>
      <Table striped bordered hover className="text-center">
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Subtotal</th>
          </tr>
        </thead>
        <tbody>
          {cartItems.map((item) => (
            <tr key={item._id}>
              <td>{item.product.name}</td>
              <td>{item.product.description}</td>
              <td>{item.product.price}</td>
              <td>{item.quantity}</td>
              <td>{item.product.price * item.quantity}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <ProductCard key={productId} productProp={{ _id: productId }} />
      <button className="btn btn-primary" onClick={addToCart}>
        Add to Cart
      </button>
    </Fragment>
  );
}