import ProductCard from "../components/ProductCard.js"
import {Fragment, useEffect, useState} from 'react'
import {Container} from 'react-bootstrap'


export default function Products(){

	const [product, setProduct] = useState([])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/`)
		.then(result => result.json())
		.then(data => {	
			setProduct(data.map(product => {
			return(
			<ProductCard key = {product._id} productProp = {product}/>
				)
			}))
		})
		.catch(error => {
			console.log(error)
		})
	}, [])

	return(
		<Fragment>
			<h1 className="text-center mt-3">Products</h1>
			<Container className="text-center">
				{product}
			</Container>
		</Fragment>
	)
}