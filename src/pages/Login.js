import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Fragment, useContext, useState, useEffect} from 'react'
import {Navigate, useNavigate} from "react-router-dom"
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function Login() {
	
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)

	const navigate = useNavigate()


	const {user, setUser} = useContext(UserContext)




	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])

	function login(event) {
		event.preventDefault()

		//if you want to add the email of the authenticated user in the local storage
			//Syntax:
				//localStorage.setItem("propertyName", value)

		//Process a fetch request to corresponding backend API
		//Syntax: fetch('url', {options})
		fetch(`${process.env.REACT_APP_API_URL}/user/authentication`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === false){
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Please try again!"
				})
			}else{
				Swal.fire({
					title: "Authentication successful",
					icon: 'success',
					text: 'Welcome to shop'
				})
				localStorage.setItem('token', data.auth)				
				retrieveUserDetails(localStorage.getItem('token'))
				navigate('/')
			}
		})

	}

	const retrieveUserDetails = (token) => {

		//the token sent as part of the request header information
		fetch(`${process.env.REACT_APP_API_URL}/user/getProfile`, {
			headers: {
				Authorization: `Bearer ${token}` 
			}
		})
		.then(result => result.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			
		})
	}



	return(
		user ?
		<Navigate to = "*"/>
		:
		<Fragment>
			<h1 className="mt-5">Login</h1>
			<Form className="mt-3" onSubmit={event => login(event)}>
			     <Form.Group className="mb-3" controlId="formBasicEmail">
			       <Form.Label>Email address</Form.Label>
			       <Form.Control 
			       		type="email" 
			       		placeholder="Enter email"
			       		value = {email}
			       		onChange = {event => setEmail(event.target.value)}
			       		required />
			     </Form.Group>

			     <Form.Group className="mb-3" controlId="formBasicPassword">
			       <Form.Label>Password</Form.Label>
			       <Form.Control 
			       		type="password" 
			       		placeholder="Password"
			       		value = {password}
			       		onChange = {event => setPassword(event.target.value)}
			       		required />
			     </Form.Group>

			     {
			     	isActive ?
			     	<Button variant="success" type="submit">
			     	  Submit
			     	</Button>
			     	:
			     	<Button variant="success" type="submit" disabled>
			     	  Submit
			     	</Button>
			     }

			    
			   </Form>
		</Fragment>
		)
}