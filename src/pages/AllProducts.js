import ProductTable from "../components/ProductTable.js"
import {Fragment, useEffect, useState} from 'react'
import {Container, Table} from 'react-bootstrap'


export default function AllProducts(){


	const [product, setProduct] = useState([])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/all`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setProduct(data.map(product => {
				return(
				<ProductTable key = {product._id} productProp = {product}/>
				)
			}))
		})
		.catch(error => {
			console.log(error)
		})
	}, [])

	return(
		<Fragment>
			<h1 className="text-center mt-3">All Products</h1>
			<Table striped bordered hover className="text-center">
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Action</th>						
					</tr>
				</thead>
				{product}
			</Table>				
		</Fragment>
	)
}