import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext.js'
import {useContext, useEffect} from 'react'

// logout user
export default function Logout (){
	const {unSetUser, setUser} = useContext(UserContext)
	useEffect(() => {
		unSetUser()
		setUser(null)
	}, [])
	
	return(
		<Navigate to ="/login"/>		
		)
}