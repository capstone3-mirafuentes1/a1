//destructuring
import {Row, Col, Button, Container} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {useContext} from 'react'
import UserContext from '../UserContext.js'

import '../home.css'

export default function Banner(){
	const {user} = useContext(UserContext)
	return(
		<div className="d-flex align-items-center justify-content-center heights">
			<Row >
				<Col className="text-center">
					<h1>Shop</h1>
					<p className="pt-1">All types of products available here</p>
					{
						user ?
						<Button as = {Link} to = '/activeProducts' className="pt-1 mb-5">Shop Now</Button>
						:
						<Button as = {Link} to = '/login' className="pt-1 mb-5">Login</Button>
					}
					
				</Col>
			</Row>
		</div>	
		)
}