import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import "../table.css"
import {Fragment, useEffect} from 'react'
import Swal from 'sweetalert2'
import {Navigate, useNavigate, Link} from "react-router-dom"


export default function ProductTable ({productProp}) {
	const {_id, name, description, price, isActive, createdOn} = productProp

	const navigate = useNavigate()
	
	function on() {
		fetch(`${process.env.REACT_APP_API_URL}/product/activate/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data !== false){
				Swal.fire({
					title: "Activate Successfully",
					icon: "success",
					text: "create product Successfully"
				})
				navigate('/activeProducts')
			}else{
				Swal.fire({
					title: "You are not an admin",
					icon: "error",
					text: "Become an admin first"
				})
			}
		})
	}

	function off(){
		fetch(`${process.env.REACT_APP_API_URL}/product/archive/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data !== false){
				Swal.fire({
					title: "Deactivate Successfully",
					icon: "success",
					text: "Wait for the output"
				})
				navigate('/activeProducts')
			}else{
				Swal.fire({
					title: "Deactivate Error",
					icon: "error",
					text: "Try again"
				})
			}
		})
	}


	return(		
		      <tbody>
		        <tr>
		          <td className="justify-content-center" data-label="Name">{name}</td>
		          <td data-label="Description">{description}</td>
		          <td data-label="Price">{price}</td>
		          <td data-label="Action">
		          {
		          	isActive ?
		          	<Fragment>
		          		<Button className="m-1" as = {Link} to = {`/update/${_id}`}>Update</Button>
		          		<Button variant="danger" onClick={event => off(event)}>Deactivate</Button>
		          	</Fragment>
		          	:
		          	<Fragment>
		          		<Button className="m-1" as = {Link} to = {`/update/${_id}`}>Update</Button>
		          		<Button variant="success" onClick={event => on(event)}>Activate</Button>
		          	</Fragment>
		          }		         	
		          </td>
		        </tr>
		      </tbody>
	)
}