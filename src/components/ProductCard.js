import Card from 'react-bootstrap/Card';
import {Row, Col, Button} from 'react-bootstrap'
import {useState} from 'react'
import {useEffect, useContext} from 'react'
import UserContext from '../UserContext.js'
import {Link} from 'react-router-dom'
import {useParams, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function ProductCard ({productProp}) {

	const {_id, name, description, price} = productProp

	const [enrollees, setEnrollees] = useState(0)


	const [seats, setSeats] = useState('')
	
	const [isDisabled, setIsDisabled] = useState(false)

	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	// function addcard(event){
	// 	event.preventDefault()
	// 	fetch(`${process.env.REACT_APP_API_URL}/order/addCart/${_id}`, {
	// 		method: "POST",
	// 		headers: {
	// 			"Content-Type": 'application/json',
	// 			Authorization: `Bearer ${localStorage.getItem("token")}`
	// 		}
	// 	})
	// 	.then(result => result.json())
	// 	.then(data =>{
	// 		if(data !== false){
	// 			Swal.fire({
	// 				title: "Successfully Added!",
	// 				icon: "success",
	// 				text: "Please wait for the final schedule of the course!"
	// 			})
	// 			navigate('/courses')
	// 		}else{
	// 			Swal.fire({
	// 				title: "Add to Cart erro",
	// 				icon: "error",
	// 				text: "Please try again!"
	// 			})
	// 		}
	// 	})
		
	// }



	return(
		<Row className="m-1 mb-3 text-center d-inline-flex col-md-3 col-12">
			<Col>
				<Card>
				      <Card.Body>
				        <Card.Title className="text-primary" size="lg">{name}</Card.Title>
				        <Card.Text>
				        	<Card.Subtitle>Description:</Card.Subtitle>
				        	<Card.Text className="">{description}</Card.Text>
				        	<Card.Subtitle>Price:</Card.Subtitle>
				        	<Card.Text>Php {price}</Card.Text>
				        </Card.Text>
				        {
				        	user ?
				        	<Button variant="btn btn-outline-dark" disabled = {isDisabled} as = {Link} to = "/order">Add to Cart</Button>
				        	:
				        	<Button variant="btn btn-outline-dark" as = {Link} to = "/login">Login</Button>
				        }
				        
				      </Card.Body>
				</Card>
			</Col>
		</Row>
		)
}