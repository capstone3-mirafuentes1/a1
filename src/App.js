import './App.css';
import AppNavBar from './components/AppNavBar.js'
import Dashboard from './components/Dashboard.js'
import Home from './pages/Home.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Logout from './pages/Logout.js'
import PageNotFound from './pages/PageNotFound.js'
import ProductView from './components/ProductView.js'
import CreateProduct from './components/CreateProduct.js'
import AllProducts from './pages/AllProducts.js'
import Products from './pages/Products.js'
import Update from './pages/Update.js'
import AddCart from './pages/AddCart.js'


import {useState, useEffect} from 'react'

//import the UserProvider
import {UserProvider} from './UserContext.js'

//import modules from react-router-dom for the routing 
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'

function App() {
  const [user, setUser] = useState({})
  
  

  const unSetUser = () => {
    localStorage.clear()
  }
  useEffect(()=> {
    fetch(`${process.env.REACT_APP_API_URL}/user/profile`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem("token")}` 
      }
    })
    .then(result => result.json())
    .then(data => {
      console.log(data)
      if(localStorage.getItem('token') !== null){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        console.log(data.isAdmin)
      }else{
        setUser(null)
      }
      
    })
  }, [])

  //storing information in a context object is done by providing the information using the corresponding "Provider" and passing information thru the prop value
  //all information/data provided to the "Provider" component can be access later on from the context object properties

  return (
   <UserProvider value={{user, setUser, unSetUser}}>
    <Router>
      <AppNavBar/>
      <Routes>
          <Route path="/" element = {<Home/>}/>
          <Route path="/activeProducts" element = {<Products/>}/>
          <Route path="/login" element = {<Login/>}/>
          <Route path="/register" element = {<Register/>}/>
          <Route path="/Logout" element = {<Logout/>}/>
          <Route path="*" element = {<PageNotFound/>}/>
          <Route path="/createProduct" element = {<CreateProduct/>}/>
          <Route path="/allProduct" element = {<AllProducts/>}/>
          <Route path="/update/:productId" element = {<Update/>}/>
          <Route path="/product/:productId" element = {<ProductView/>}/>
          <Route path="/order" element = {<AddCart/>}/>
      </Routes>
    </Router>
   </UserProvider>
  );
}

export default App;
